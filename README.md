### Description 
This repository is for participants of Lab RSE 2021's training to work on tutorials. This repository contains all files related to IFML Generator: a generator that converts IFML into an angular based project. 

### Preparation
These are the dependencies/tools that you need:
1. Node 10.16.3 (https://nodejs.org/ca/blog/release/v10.16.3/)
2. NPM 6.9.0 (https://www.npmjs.com/package/npm/v/6.9.0)
3. Python 3.7 (https://www.python.org/downloads/)
4. Nginx based on Current OS (http://nginx.org/en/download.html)
5. Eclipse versi 2020-09 (https://www.eclipse.org/downloads/packages/release/2020-09)
6. Add On IFML (http://ifml.github.io/)

### How To Run on Your Local?
###### 1. Cloning the repo
`git clone https://gitlab.com/RSE-Lab-Fasilkom-UI/training2021/ifml-tutorial.git`
###### 2. Create and activate virtualenv at the root project (For more, see this: https://gist.github.com/Geoyi/d9fab4f609e9f75941946be45000632b)
`virtualenv venv`\
`cd venv\Scripts` (entering scripts folder)\
`activate`
###### 3. Create new Product
Open yout terminal, point to root project.
`python __init__.py financial.json angular`
Explanation:
We run a python file `__init__.py` with 2 arguments, first is `financial.json` (a json that contains the specification for new product, you can modify or create the new one), second is `angular` that means we want to generate angular project.\

In `financial.json` there are selected features. Every feature has map to core module and an uml. So, you have to map your new core module and an uml to a new feature. The mapping configuration exists in `mapping.json`

###### 4. Running the project
If product created, you will see the result at folder `result\[your_product_name]`
Don't forget to activate yout virtualenv!
* Point your terminal to product folder
`cd result\[your_product_name]`
* Install the project dependencies (don't forget to install yarn, for windows user, see this https://classic.yarnpkg.com/en/docs/install/#windows-stable or another source)
`yarn install`
* run the project
 `yarn start`
* Open your browser at `http://localhost:4200/`
###### 5. Replace the css file
In your browser, you will see a not good frontend, because the css is very simple. So, you can replace the css with the file names `new_css.css` at the root project. Replace it to `result\[your_product_name]\src\styles.css` 
Replace your browser to see the changes :) 
