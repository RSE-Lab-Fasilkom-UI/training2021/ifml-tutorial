from main.utils.ast.base import Node
from main.utils.jinja.angular import base_file_writer, app_components_writer


class LoginTypescriptClass(Node):

    def __init__(self, url, domain_name, client_id, client_secret):
        super().__init__()
        self.selector_name = 'login'
        self.class_name = 'Login'
        self.url = url
        self.domain_name = domain_name
        self.client_id = client_id
        self.client_secret = client_secret

    def render(self):
        return app_components_writer('login/login.component.ts.template', url=self.url, domain_name=self.domain_name,
                                     client_id=self.client_id, client_secret=self.client_secret)

    def get_class_name(self):
        return self.class_name


class LoginHTML(Node):

    def __init__(self):
        super().__init__()

    def render(self):
        return base_file_writer('src/app/login/login.component.html.template')
